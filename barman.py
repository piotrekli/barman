#!/bin/python2

import sys, ConfigParser, time, itertools, os, json, threading

from json import dumps
from importlib import import_module

class Barman(object):
  def __init__(self):
    self.ups = 6
    self.blocks = [] # elements are lists: [block, sleep, text, lock]
    self.out = sys.stdout
    self.evtin = sys.stdin
    self.options = {'click_events': True}
    self.click_evt_thread = None
    self.bar_update_lock = threading.Lock()
    self.default_block_options = {}
    self.update_after_event = True
  def add_block(self, block):
    self.blocks.append([block, 0, '', threading.Lock()])
  def create_block(self, mod, cls, opts, custom_opts):
    try:
      mod = import_module(mod, './')
      blk = getattr(mod, cls)()
      blk.options.update(opts)
      blk.options['instance']='barman-'+str(len(self.blocks))
      blk.set_custom_options(**custom_opts)
      blk.pre_init_block()
      blk.init_block()
      self.add_block(blk)
    except BaseException as e:
      sys.stderr.write('Error creating block '+`mod`+': '+`e`+'\n')
  def tick(self, block=None, tick=True):
    changed = False
    data = []
    blkid = 0
    for blk in self.blocks:
      blk[3].acquire()
      if block == None or block == blkid:
        try:
          if tick:
            if blk[1] == 0:
              blk[1] = int(blk[0].tick(self.ups))
            if blk[1] > 0:
              blk[1] -= 1
            blk[0].update_scroll()
          text = blk[0].update(True)
          if text != None:
            changed = True
            blk[2] = text
        except BaseException as e:
          sys.stderr.write('Error updating block: '+`e`+'\n')
      if blk[2] not in (None, '', '\n', '{}\n'):
        data.append(blk[2])
      blk[3].release()
      blkid += 1
    if changed:
      return data
  def update_bar(self, data):
    bar = ['[\n']
    comma = False
    for i in data:
      if comma:
        bar.append(', ')
      comma = True
      bar.append(i)
    bar.append('],\n')
    self.bar_update_lock.acquire()
    self.out.write(''.join(bar))
    self.out.flush()
    self.bar_update_lock.release()
  def run_events(self):
    while True:
      d = self.evtin.readline()
      if d.startswith(','):
        d = d[1:]
      try:
        evt = json.loads(d)
      except ValueError as e:
        sys.stderr.write('Error parsing event: '+`e`+'\n')
        continue
      inst = evt['instance']
      if inst.startswith('barman-'):
        inst = inst[7:]
        sub_idx = inst.find('-')
        sub = None
        if sub_idx >= 0:
          sub = parse(inst[sub_idx+1:])
          inst = inst[:sub_idx]
        try:
          inst = int(inst)
        except ValueError:
          inst = None
        if inst != None:
          blk = self.blocks[inst]
          blk[3].acquire()
          try:
            should_update = blk[0].on_click(evt['button'], sub, \
                                            self.update_after_event)
          finally:
            blk[3].release()
          if should_update:
            if self.update_after_event:
              data = self.tick(inst, False)
              if data != None:
                self.update_bar(data)
            #else:
            #  blk[3].acquire()
            #  blk[1] = 0
            #  blk[3].release()
  def run(self):
    header = ['{"version":1']
    for i in self.options.iteritems():
      header.append(', ')
      header.append(dumps(i[0]))
      header.append(':')
      header.append(dumps(i[1]))
    header.append('}\n[\n')
    self.out.write(''.join(header))
    self.out.flush()
    del header
    if 'click_events' in self.options and self.options['click_events']:
      self.click_evt_thread = threading.Thread(target=self.run_events)
      self.click_evt_thread.daemon = True
      self.click_evt_thread.start()
    while True:
      data = self.tick()
      if data != None:
        self.update_bar(data)
      time.sleep(1.0/self.ups)
  def parse_config(self, cf):
    def unknown_option(opt):
      sys.stderr.write('Unknown option: '+opt+'\n')
    def parse(opt):
      if opt == '_true':
        return True
      if opt == '_false':
        return False
      if opt.startswith('_sp_'):
        return ' '+opt[4:]
      if opt.startswith('$'):
        return opt[1:]
      try:
        return int(opt)
      except ValueError:
        pass
      try:
        return float(opt)
      except ValueError:
        pass
      return opt
    conf = ConfigParser.SafeConfigParser()
    conf.readfp(cf)
    OPT_PREFIX = 'option.'
    DEFAULT_SECTION = '_default'
    BAR_SECTION = '_bar'
    if conf.has_section(BAR_SECTION):
      for opt in conf.items(BAR_SECTION):
        if opt[0].startswith(OPT_PREFIX):
          self.options[opt[0][len(OPT_PREFIX):]] = parse(opt[1])
        elif opt[0] == 'ups':
          self.ups = float(opt[1])
        elif opt[0] == 'module_path':
          os.chdir(os.path.expanduser(str(opt[1])))
        elif opt[0] == 'update_after_event':
          self.update_after_event = parse(opt[1])
        else:
          unknown_option(opt[0])
    if conf.has_section(DEFAULT_SECTION):
      for opt in conf.items(DEFAULT_SECTION):
        self.default_block_options[opt[0]] = opt[1]
    for section in conf.sections():
      if section == DEFAULT_SECTION or section == BAR_SECTION:
        continue
      mod_name = section
      blk_options = {}
      blk_custom_opts = {}
      blk_class = 'BARMAN_BLOCK'
      for opt in itertools.chain( \
        self.default_block_options.iteritems(), \
        conf.items(section)):
        if opt[0].startswith(OPT_PREFIX):
          blk_options[opt[0][len(OPT_PREFIX):]] = parse(opt[1])
        elif opt[0] == 'module':
          mod_name = str(opt[1])
        elif opt[0] == 'class':
          mod_class = str(opt[1])
        else:
          blk_custom_opts[opt[0]] = parse(opt[1])
      self.create_block(mod_name, blk_class, \
                        blk_options, blk_custom_opts)

if __name__ == '__main__':
  conf_fn = '~/.barman'
  if len(sys.argv) > 1:
    conf_fn = sys.argv[0]
  barman = Barman()
  conf = open(os.path.expanduser(conf_fn), 'rb')
  barman.parse_config(conf)
  conf.close()
  barman.run()
