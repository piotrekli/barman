#!/usr/bin/python2

import blockutil, sys

def ft(n):
  return str(int(n)).rjust(3)+'%'

def getv(f, t):
  f.seek(0)
  s = f.read()
  return t(s)

class BatteryBlock(blockutil.StyledBlock):
  def __init__(self, use_json=True):
    blockutil.StyledBlock.__init__(self, use_json)
    self.ICON_CHARGING    = '\xf0\x9f\x94\x8c'
    self.ICON_DISCHARGING = '\xf0\x9f\x94\x8b'
    self.MAIN_COLOR = '#a050c0'
    self.SEP_COLOR  = '#803090'
    self.ICON_THRESHOLD = 50
    self.battery_name = 'BAT1'
    self.battery_path = '/sys/class/power_supply/'
    self.battery_status_file = None
    self.battery_charge_file = None
    self.max_charge = -1
    self.editable_attrs.update({'battery_name', 'battery_path'})
  def init_block(self):
    self.full_text_inner.append('XXXX')
    self.set_icon_color(self.MAIN_COLOR)
    self.set_icon(self.ICON_CHARGING)
    bf = self.battery_path + self.battery_name + '/'
    self.battery_status_file = open(bf+'status', 'rb')
    self.battery_charge_file = open(bf+'charge_now', 'rb')
    battery_full_charge_file = open(bf+'charge_full', 'rb')
    self.max_charge = getv(battery_full_charge_file, int)
    battery_full_charge_file.close()
    self.enable()
  def tick(self, ups):
    status = getv(self.battery_status_file, str).rstrip()
    self.set_icon(     self.ICON_DISCHARGING if status == 'Discharging'
                  else self.ICON_CHARGING)
    self.full_text_inner[0].set_content(
      ft(getv(self.battery_charge_file, int)*100.0/self.max_charge))
    return 4

if __name__ == '__main__':
  import time, sys
  block = BatteryBlock(False)
  block.main(1, sys.stdout)

BARMAN_BLOCK = BatteryBlock
