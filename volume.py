#!/usr/bin/python2

import alsaaudio, blockutil

def ft(n):
  return str(n).rjust(3)+'%'

class VolumeBlock(blockutil.StyledBlock):
  def __init__(self, use_json=True):
    blockutil.StyledBlock.__init__(self, use_json)
    self.ICON_0 = '\xf0\x9f\x94\x88'
    self.ICON_1 = '\xf0\x9f\x94\x89'
    self.ICON_2 = '\xf0\x9f\x94\x8a'
    self.MAIN_COLOR = '#998888'
    self.SEP_COLOR  = '#444444'
    self.ICON_THRESHOLD = 50
    self.mixer_name = None
    self.editable_attrs.update({'mixer_name'})
  def init_block(self):
    self.full_text_inner.append('XXXX')
    self.set_icon_color(self.MAIN_COLOR)
    self.set_icon(self.ICON_0)
    self.enable()
  def get_mixer(self):
    if self.mixer_name == None:
      return alsaaudio.Mixer()
    else:
      return alsaaudio.Mixer(self.mixer_name)
  def on_click(self, button, subblk, should_tick=True):
    if button == 1:
      mixer = self.get_mixer()
      mixer.setmute(not all(mixer.getmute()))
      if should_tick:
        self.tick(0)
      return True
    elif button == 4 or button == 5:
      mixer = self.get_mixer()
      d = -1 if button == 5 else 1
      vol = map(lambda x: max(0, min(x+d, 100)), mixer.getvolume())
      for ch in xrange(len(vol)):
        mixer.setvolume(vol[ch])
      if should_tick:
        self.tick(0)
      return True
    return False
  def tick(self, ups):
    mixer = self.get_mixer()
    s = 'MUTE'
    i = self.ICON_0
    if not all(mixer.getmute()):
      vol = mixer.getvolume()
      vol = sum(vol)/len(vol)
      s = ft(vol)
      if vol > 0:
        i = self.ICON_2 if vol >= self.ICON_THRESHOLD else self.ICON_1
    self.full_text_inner[0].set_content(s)
    self.set_icon(i)
    return 1

if __name__ == '__main__':
  import time, sys
  block = VolumeBlock(False)
  block.main(1, sys.stdout)

BARMAN_BLOCK = VolumeBlock
