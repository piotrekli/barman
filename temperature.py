#!/usr/bin/python2

import blockutil, sys

def getv(f, t):
  f.seek(0)
  s = f.read()
  return t(s)

class TemperatureBlock(blockutil.StyledBlock):
  def __init__(self, use_json=True):
    blockutil.StyledBlock.__init__(self, use_json)
    self.ICON       = '\xf0\x9f\x8c\xa1'
    self.MAIN_COLOR = '#ee2222'
    self.SEP_COLOR  = '#aa0000'
    self.thermal_zones = []
    self.thermal_zone_icons = []
    self.thermal_path = '/sys/class/thermal/thermal_zone'
    self.temperature_files = []
    self.show_decimal_point = True
    self.editable_attrs.update({'thermal_path', 'show_decimal_point'})
  def fmt(self, n):
    n = (n+50) / 100
    fraction = '.'+str(n%10) if self.show_decimal_point else ''
    return str(n/10).rjust(3)+fraction+'\xc2\xb0C'
  def init_block(self):
    self.set_icon_color(self.MAIN_COLOR)
    self.set_icon(self.ICON)
    for i in xrange(len(self.thermal_zones)):
      self.full_text_inner.append('XXXX')
      icon = self.thermal_zone_icons[i]
      icon = '' if icon == None else ' '+icon
      self.full_text_inner.append(icon,
                                  lang='uf_UF', foreground=self.SEP_COLOR)
      bf = self.thermal_path + self.thermal_zones[i] + '/'
      self.temperature_files[i] = open(bf+'temp', 'rb')
    self.enable()
  def tick(self, ups):
    for i in xrange(len(self.thermal_zones)):
      self.full_text_inner[i*2].set_content(
        self.fmt(getv(self.temperature_files[i], int)))
    return 4
  def set_custom_options(self, **kw):
    blockutil.StyledBlock.set_custom_options(self, **kw)
    if 'zone_ids' in kw:
      a = kw['zone_ids']
      self.thermal_zones = a.split(',')
    if 'zone_icons' in kw:
      a = kw['zone_icons']
      self.thermal_zone_icons = a.split(',')
    n_missing_icons = len(self.thermal_zones) - len(self.thermal_zone_icons)
    if n_missing_icons > 0:
      self.thermal_zone_icons += [None] * n_missing_icons
    self.temperature_files = [None] * len(self.thermal_zones)

if __name__ == '__main__':
  import time, sys
  block = TemperatureBlock(False)
  block.main(1, sys.stdout)

BARMAN_BLOCK = TemperatureBlock
