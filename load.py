#!/usr/bin/python2

import os, psutil, blockutil

def ft(n):
  return str(int(n)).rjust(3)+'% '

class LoadBlock(blockutil.StyledBlock):
  def __init__(self, use_json=True):
    blockutil.StyledBlock.__init__(self, use_json)
    self.n_cores = psutil.cpu_count()
    self.ICON = '\xe4\xb7\x80'
    self.LOAD_ICON = '\xe2\x9a\x96'
    self.CPU_ICON  = '\xe2\x8f\xb1'
    self.FREQ_ICON = '\xe2\x9a\x99'
    self.MAIN_COLOR = '#e2a333'
    self.SEP_COLOR  = '#a16111'
    self.normalize_load = True
    self.show_freq = False
    self.editable_attrs.update(['normalize_load', 'show_freq'])
  def init_block(self):
    self.full_text_inner.append('XXX')
    self.full_text_inner.append(self.LOAD_ICON,
                                lang='uf_UF', foreground=self.SEP_COLOR)
    self.full_text_inner.append('XXX')
    self.full_text_inner.append(self.CPU_ICON,
                                lang='uf_UF', foreground=self.SEP_COLOR)
    if self.show_freq:
      self.full_text_inner.append('XXXXHz')
      self.full_text_inner.append(self.FREQ_ICON,
                                  lang='uf_UF', foreground=self.SEP_COLOR)
    self.set_icon_color(self.MAIN_COLOR)
    self.set_icon(self.ICON)
    if not self.normalize_load:
      self.n_cores = 1
    self.enable()
  def tick(self, ups):
    self.full_text_inner[0].set_content(ft(os.getloadavg()[0]/self.n_cores*100))
    self.full_text_inner[2].set_content(ft(psutil.cpu_percent()))
    if self.show_freq:
      self.full_text_inner[4].set_content(
        ' '+self.fmt_size(psutil.cpu_freq().current*1000000, 1000)+'Hz ')
    return 2
    
if __name__ == '__main__':
  import time, sys
  block = LoadBlock(False)
  block.main(1, sys.stdout)

BARMAN_BLOCK = LoadBlock
