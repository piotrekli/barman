#!/usr/bin/python2

import os, psutil, blockutil

class MemBlock(blockutil.StyledBlock):
  def __init__(self, use_json=True):
    blockutil.StyledBlock.__init__(self, use_json)
    self.ICON = '\xf0\x9f\x93\x9a'
    self.USED_ICON  = '\xe2\x9c\xa8'
    self.CACHE_ICON = '\xe2\x9b\x9f'
    self.SWAP_ICON  = '\xe2\x8e\x97'
    self.MAIN_COLOR = '#e24343'
    self.SEP_COLOR  = '#a11111'
  def init_block(self):
    self.full_text_inner.append('XXX')
    self.full_text_inner.append(self.USED_ICON+' ',
                                lang='uf_UF', foreground=self.SEP_COLOR)
    self.full_text_inner.append('XXX')
    self.full_text_inner.append(self.CACHE_ICON,
                                lang='uf_UF', foreground=self.SEP_COLOR)
    self.set_icon_color(self.MAIN_COLOR)
    self.set_icon(self.ICON)
    self.enable()
  def tick(self, ups):
    ram = psutil.virtual_memory()
    swap = psutil.swap_memory()
    self.full_text_inner[0].set_content(self.fmt_size(ram.total-ram.free-ram.buffers-ram.cached)+' ')
    self.full_text_inner[2].set_content(self.fmt_size(ram.buffers+ram.cached)+' ')
    if swap.total > 0:
      if len(self.full_text_inner) <= 4:
        self.full_text_inner.append(' XXX', foreground=self.MAIN_COLOR)
        self.full_text_inner.append(self.SWAP_ICON, lang='uf_UF', foreground=self.SEP_COLOR)
      self.full_text_inner[4].set_content(' '+self.fmt_size(swap.used)+' ')
    else:
      if len(self.full_text_inner) > 4:
        del self.full_text_inner[4]
        del self.full_text_inner[3]
    return ups
    
if __name__ == '__main__':
  import time, sys
  block = MemBlock(False)
  block.main(1, sys.stdout)

BARMAN_BLOCK = MemBlock
