#!/usr/bin/python2

import time, psutil, blockutil, socket

class NetBlock(blockutil.StyledBlock):
  def __init__(self, use_json=True):
    blockutil.StyledBlock.__init__(self, use_json)
    self.ICON = '\xe2\x9b\x93'
    self.MAIN_COLOR = '#7ec04a'
    self.SEP_COLOR  = '#419023'
    self.UP_ICON_ACTIVE     = '\xe2\xac\x86'
    self.UP_ICON_INACTIVE   = '\xe2\x87\xa7'
    self.DOWN_ICON_ACTIVE   = '\xe2\xac\x87'
    self.DOWN_ICON_INACTIVE = '\xe2\x87\xa9'
    self.iface = None
    self.prev_time = time.time()
    self.prev_net = None
    self.iir_uload = 0.0
    self.iir_dload = 0.0
    self.iir_exp = 0.5
    self.addr_timer = 0
    self.addr_freq = 30
    self.addr_family = socket.AF_INET
    self.editable_attrs.update({'iface', 'iir_exp', 'addr_freq'})
  def init_block(self):
    self.full_text_inner.append('XXX')
    self.full_text_inner.append(self.UP_ICON_INACTIVE+' ',
                                lang='uf_UF', foreground=self.SEP_COLOR)
    self.full_text_inner.append('XXX')
    self.full_text_inner.append(self.DOWN_ICON_INACTIVE+' ',
                                lang='uf_UF', foreground=self.SEP_COLOR)
    self.set_icon_color(self.MAIN_COLOR)
    self.set_icon(self.ICON)
    self.enable()
  def tick(self, ups):
    now = time.time()
    dt = now - self.prev_time
    self.prev_time = now
    net = psutil.net_io_counters(pernic=self.iface != None, nowrap=True)
    if self.iface != None:
      if self.iface not in net:
        self.disable()
        self.addr_timer = 0
        return ups*10
      else:
        self.enable()
        net = net[self.iface]
    if self.prev_net == None:
      self.prev_net = net
    uload = (net.bytes_sent-self.prev_net.bytes_sent)/dt
    dload = (net.bytes_recv-self.prev_net.bytes_recv)/dt
    self.iir_dload = dload*self.iir_exp+self.iir_dload*(1-self.iir_exp)
    self.iir_uload = uload*self.iir_exp+self.iir_uload*(1-self.iir_exp)
    self.full_text_inner[0].set_content(self.fmt_size(self.iir_uload)+'/s ')
    self.full_text_inner[1].set_content((self.UP_ICON_ACTIVE if uload > 0 \
                                         else self.UP_ICON_INACTIVE)+' ')
    self.full_text_inner[2].set_content(self.fmt_size(self.iir_dload)+'/s ')
    self.full_text_inner[3].set_content((self.DOWN_ICON_ACTIVE if dload > 0 \
                                         else self.DOWN_ICON_INACTIVE))
    if self.addr_family >= 0 and self.iface != None:
      if self.addr_timer <= 0:
        self.addr_timer = self.addr_freq
        addr = None
        addrs = psutil.net_if_addrs()
        if self.iface in addrs:
          addrs = addrs[self.iface]
          for i in addrs:
            if i.family == self.addr_family:
              addr = i.address
              break
        if addr == None:
          if len(self.full_text_inner) > 4:
            del self.full_text_inner[4]
        else:
          if len(self.full_text_inner) <= 4:
            self.full_text_inner.append(' '+addr)
          else:
            self.full_text_inner[4].set_content(' '+addr)
      else:
        self.addr_timer -= 1
    self.prev_net = net
    return 1
  def set_custom_options(self, **kw):
    blockutil.StyledBlock.set_custom_options(self, **kw)
    if 'addr_family' in kw:
      a = kw['addr_family']
      families = {'ipv4': socket.AF_INET, 'ipv6': socket.AF_INET6,
                  'link': psutil.AF_LINK}
      self.addr_family = families[a]

if __name__ == '__main__':
  import time, sys
  block = NetBlock(False)
  block.main(1, sys.stdout)

BARMAN_BLOCK = NetBlock
