#!/usr/bin/python2

import time, blockutil

def ft(n):
  return str(n).rjust(2, '0')

class ClockBlock(blockutil.StyledBlock):
  def __init__(self, use_json=True):
    blockutil.StyledBlock.__init__(self, use_json)
    self.ICON = '\xe2\x8f\xb0'
    self.MAIN_COLOR = '#abcdef'
    self.SEP_COLOR  = '#56789a'
    self.SEP = ':'
  def init_block(self):
    self.full_text_inner.append('HH')
    self.full_text_inner.append(self.SEP, foreground=self.SEP_COLOR)
    self.full_text_inner.append('MM')
    self.full_text_inner.append(self.SEP, foreground=self.SEP_COLOR)
    self.full_text_inner.append('SS')
    self.set_icon_color(self.MAIN_COLOR)
    self.set_icon(self.ICON)
    self.enable()
  def tick(self, ups):
    lt = time.localtime()
    self.full_text_inner[0].set_content(ft(lt.tm_hour))
    self.full_text_inner[2].set_content(ft(lt.tm_min))
    self.full_text_inner[4].set_content(ft(lt.tm_sec))
    return ups
    
if __name__ == '__main__':
  import time, sys
  block = ClockBlock(False)
  block.main(1, sys.stdout)

BARMAN_BLOCK = ClockBlock
