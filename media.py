#!/usr/bin/python2

import dbus, blockutil

IFACE_NAME = 'org.mpris.MediaPlayer2.Player'

def nstr(x):
  if x == None:
    return None
  return unicode(x)

class MediaPlayer(object):
  Status = type('MediaPlayer.Status', (), {"STOPPED": 0, "PLAYING": 1, "PAUSED": 2, "OTHER": 3})
  def __init__(self, name):
    bus = dbus.SessionBus()
    obj = bus.get_object('org.mpris.MediaPlayer2.'+name, '/org/mpris/MediaPlayer2')
    self.iface = dbus.Interface(obj, IFACE_NAME)
    self.props = dbus.Interface(obj, 'org.freedesktop.DBus.Properties')
  def play(self):
    self.iface.Play()
  def pause(self):
    self.iface.Pause()
  def playpause(self):
    self.iface.PlayPause()
  def next_track(self):
    self.iface.Next()
  def prev_track(self):
    self.iface.Previous()
  def get_volume(self):
    return self.props.Get(IFACE_NAME, 'Volume').real
  def set_volume(self, value):
    self.props.Set(IFACE_NAME, 'Volume', value)
  def add_volume(self, delta):
    self.set_volume(self.get_volume()+delta)
  def get_position(self):
    return self.props.Get(IFACE_NAME, 'Position').real
  def get_length(self, meta=None):
    if meta == None:
      meta = self.props.Get(IFACE_NAME, 'Metadata')
    length = meta.get('mpris:length')
    if length != None:
      return length.real
    return
  def get_track_info(self):
    meta = self.props.Get(IFACE_NAME, 'Metadata')
    info = {}
    info['title'] = nstr(meta.get('xesam:title'))
    info['album'] = nstr(meta.get('xesam:album'))
    artist_array = meta.get('xesam:artist')
    if artist_array != None:
      info['artist'] = nstr(artist_array[0])
    else:
      info['artist'] = None
    info['length'] = self.get_length(meta)
    info['art'] = nstr(meta.get('mpris:artUrl'))
    info['url'] = nstr(meta.get('xesam:url'))
    return info
  def get_status(self):
    status = str(self.props.Get(IFACE_NAME, 'PlaybackStatus'))
    if status == 'Playing':
      return MediaPlayer.Status.PLAYING
    elif status == 'Paused':
      return MediaPlayer.Status.PAUSED
    elif status == 'Stopped':
      return MediaPlayer.Status.STOPPED
    else:
      return MediaPlayer.Status.OTHER

class MediaBlock(blockutil.StyledBlock):
  def __init__(self, use_json=True):
    blockutil.StyledBlock.__init__(self, use_json)
    self.ICON_PLAYING = '\xe2\x8f\xb5' #'\xe2\x99\xac'
    self.ICON_PAUSED  = '\xe2\x8f\xb8' #'\xe2\x99\xab'
    self.MAIN_COLOR = '#55bbff'
    self.SEP_COLOR  = '#0070c0'
    self.scroll_enabled = True
    self.max_length = 30
    self.player = None
    self.player_name = 'vlc'
    self.old_title = old_artist = None
    self.editable_attrs.update({'max_length', 'player_name'})
  def init_block(self):
    self.full_text_inner.append('', foreground=self.SEP_COLOR) # artist
    self.full_text_inner.append('') # title
    self.set_icon_color(self.MAIN_COLOR)
  def on_click(self, button, subblk, should_tick=True):
    if self.player != None:
      done = True
      try:
        if   button == 1:
          self.player.playpause()
        elif button == 4:
          self.player.prev_track()
        elif button == 5:
          self.player.next_track()
        else:
          done = False
      except dbus.DBusException:
        done = False
      if done:
        if should_tick:
          self.tick(0)
        return True
    return False
  def tick(self, ups):
    try:
      if self.player == None:
        self.player = MediaPlayer(self.player_name)
      status = self.player.get_status()
      if status == MediaPlayer.Status.STOPPED:
        info = None
        self.disable()
      else:
        info = self.player.get_track_info()
    except dbus.DBusException as e:
      # print e
      self.player = None
      self.disable()
      return ups*4
    if info != None:
      self.enable()
      artist = None
      title = info['title']
      if title == None:
        title = info['url']
      if title == None:
        title = '[unknown]'
      if info['artist'] != None:
        artist = info['artist']+' - '
      else:
        artist = ''
      self.full_text_inner[0].set_content(artist)
      self.full_text_inner[1].set_content(title)
      if title != self.old_title or artist != self.old_artist:
        self.reset_scroll()
      self.old_title = title
      self.old_artist = artist
      self.set_icon(self.ICON_PAUSED if status==MediaPlayer.Status.PAUSED else self.ICON_PLAYING)
    return 1
    
if __name__ == '__main__':
  import time, sys
  block = MediaBlock(False)
  block.main(5, sys.stdout)

BARMAN_BLOCK = MediaBlock
