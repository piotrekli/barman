#!/usr/bin/python2

import blockutil

def tohex(s):
  return ''.join(map(lambda x:'%02X'%(ord(x),), s))

class RandomBlock(blockutil.StyledBlock):
  def __init__(self, use_json=True):
    blockutil.StyledBlock.__init__(self, use_json)
    self.ICON = '\xf0\x9f\x8e\xb2'
    self.MAIN_COLOR = '#ee6a22'
    self.SEP_COLOR  = '#aa7711'
    self.filename = '/dev/urandom'
    self.nbytes = 4
    self.editable_attrs.update(['filename', 'nbytes'])
  def init_block(self):
    self.full_text_inner.append('XXXXXXXX')
    self.source = open(self.filename, 'rb')
    self.set_icon_color(self.MAIN_COLOR)
    self.set_icon(self.ICON)
    self.enable()
  def tick(self, ups):
    self.full_text_inner[0].set_content(tohex(self.source.read(self.nbytes)))
    return 10
    
if __name__ == '__main__':
  import time, sys
  block = RandomBlock(False)
  block.main(1, sys.stdout)

BARMAN_BLOCK = RandomBlock
