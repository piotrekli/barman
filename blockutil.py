#!/bin/python2

from json import dumps
from cgi import escape as xmlesc
import sys

class MarkupItemOwnerObj(object):
  def __init__(self, item):
    self.item = item
  def mark_dirty(self):
    self.item.cache[1] = None
    self.item.get_array()
    self.item.mark_dirty()

class MarkupItem(object):
  def __init__(self, s, mtype, kw):
    self.content_owner = MarkupItemOwnerObj(self)
    self.content = s
    if hasattr(self.content, 'set_owner'):
      self.content.set_owner(self.content_owner)
    self.mtype = mtype
    self.args = kw
    self.cache = [None, None, None]
    self.owner = None # used to notify about changes (mark_dirty)
  def transform_content(self):
    c = self.content
    if not isinstance(self.content, unicode):
      c = str(c).decode('utf-8')
    return c
  def get_array(self):
    if self.cache[0] == None:
      if self.mtype != None:
        l = []
        l.append('<'+self.mtype)
        if self.args != None:
          for arg in self.args.iteritems():
            l.append(' '+xmlesc(arg[0])+'="'+xmlesc(arg[1])+'"')
        l.append('>')
        self.cache[0] = ''.join(l)
      else:
        self.cache[0] = ''
    if self.cache[1] == None:
      if hasattr(self.content, 'get_array'):
        self.cache[1] = list(self.content.get_array())
      else:
        c = self.transform_content()
        self.cache[1] = [xmlesc(c)]
    if self.cache[2] == None:
      if self.mtype != None:
        self.cache[2] = '</'+self.mtype+'>'
      else:
        self.cache[2] = ''
    return tuple([self.cache[0]]+self.cache[1]+[self.cache[2]])
  def get_array_sliced(self, start, end):
    got = self.get_array()
    l = [got[0]]
    if hasattr(self.content, 'get_array_sliced'):
      l.extend(self.content.get_array_sliced(start, end))
    elif hasattr(self.content, 'get_string_sliced'):
      l.append(self.content.get_string_sliced(start, end))
    else:
      l.append(xmlesc(self.transform_content()[start:end]))
    l.append(got[-1])
    return l
  def get_string_sliced(self, start, end):
    return ''.join(self.get_array_sliced(start, end))
  def get_string(self):
    return ''.join(self.get_array())
  def get_content(self):
    return self.content
  def set_content(self, content):
    changed = content != self.content
    self.content = content
    if changed:
      self.cache[1] = None
      if hasattr(self.content, 'set_owner'):
        self.content.set_owner(self.content_owner)
      self.mark_dirty()
  def get_mtype(self):
    return self.mtype
  def set_mtype(self, mtype):
    changed = mtype != self.mtype
    self.mtype = mtype
    if changed:
      self.cache[0] = self.cache[2] = None
      self.mark_dirty()
  def get_arg(self, idx):
    return self.args[idx]
  def set_arg(self, idx, val):
    if val == None:
      if self.args.has_key(idx):
        self.args.remove(idx)
        self.cache[0] = None
        self.mark_dirty()
    else:
      changed = True
      if self.args.has_key(idx):
        changed = self.args[idx] != val
      self.args[idx] = val
      if changed:
        self.cache[0] = None
        self.mark_dirty()
  def nchars(self):
    if hasattr(self.content, 'nchars'):
      return self.content.nchars()
    else:
      return len(self.content)
  def mark_dirty(self):
    if self.owner != None:
      self.owner.mark_dirty()
  def set_owner(self, owner):
    self.owner = owner
  def __str__(self):
    return str(self.content)
  def __repr__(self):
    return 'MarkupItem('+str(self)+')'

class Markup(object):
  def __init__(self, s=None):
    self.parts = []
    if s != None:
      self.append(s)
    self.owner = None
  def insert(self, index, s, mtype=None, **kw):
    if mtype == None and len(kw) > 0:
      mtype = 'span'
    item = MarkupItem(s, mtype, kw)
    self.parts.insert(index, item)
    item.set_owner(self)
    self.mark_dirty()
  def append(self, s, mtype=None, **kw):
    self.insert(len(self), s, mtype, **kw)
  def nchars(self):
    return sum([x.nchars() for x in self.parts])
  def get_array_sliced(self, start, end):
    l = []
    pos = 0
    for i in self.parts:
      astart = aend = None
      nch = i.nchars()
      if pos+nch > start:
        astart = max(0, start-pos)
      if pos < end:
        aend = min(end-pos, nch)
      if astart != None and aend != None:
        l.extend(i.get_array_sliced(astart, aend))
      pos += nch
    return l
  def set_owner(self, owner):
    self.owner = owner
  def mark_dirty(self):
    if self.owner != None:
      self.owner.mark_dirty()
  def get_array(self):
    s = []
    for i in self.parts:
      s.extend(i.get_array())
    return s
  def get_string(self):
    return ''.join(self.get_array())
  def __str__(self):
    return self.get_string()
  def __repr__(self):
    return 'Markup('+''.join([x.get_content() for x in self.parts])+')'
  def __len__(self):
    return len(self.parts)
  def __getitem__(self, key):
    return self.parts[key]
  def __setitem__(self, key, value):
    self.parts[key] = value
    value.set_owner(self)
    self.mark_dirty()
  def __delitem__(self, key):
    self.parts.pop(key)
    self.mark_dirty()

class DirtyFlag(object):
  def __init__(self):
    self.flag = True
    self.parent = None
  def mark_dirty(self):
    self.flag = True
    if self.parent != None:
      self.parent.mark_dirty()
  def set_dirty(self, dirty):
    self.flag = dirty
  def __nonzero__(self):
    return self.flag

class Block(object):
  def __init__(self, use_markup, use_json=True):
    self.dirty_flag = DirtyFlag()
    self.enabled = False
    self.use_json = use_json
    self.use_markup = use_markup
    self.full_text_prefix = self.init_markup()
    self.full_text_inner = self.init_markup()
    self.full_text_suffix = self.init_markup()
    self.short_text = None
    self.prefix_enabled = True
    self.suffix_enabled = True
    self.margin_left = ''
    self.margin_right = ''
    self.options = {}
    if use_markup:
      self.options['markup'] = 'pango'
    self.scroll_speed = 2
    self.scroll_wait_time = [100, 25, 20]
    self.scroll_enabled = False
    self.reset_scroll()
    self.max_length = 0
    self.sub_blocks = []
  def init_block(self):
    pass
  def pre_init_block(self):
    pass
  def enable(self):
    if not self.enabled:
      self.mark_dirty()
    self.enabled = True
  def disable(self):
    if self.enabled:
      self.mark_dirty()
    self.enabled = False
  def add_sub_block(self, block):
    self.sub_blocks.append(block)
    block.dirty_flag.parent = self
    block.options['instance'] = \
      self['instance']+'-'+str(len(self.sub_blocks))
    self.mark_dirty()
  def init_markup(self):
    if not self.use_markup:
      return ''
    m = Markup()
    m.set_owner(self.dirty_flag)
    return m
  def mark_dirty(self):
    self.dirty_flag.mark_dirty()
  def reset_scroll(self):
    self.scroll_ticks = -self.scroll_wait_time[-1]
    self.scroll_pos = 0
    self.scroll_dir = 0
  def calc_scroll_slice(self, nchars):
    if self.scroll_dir == 0:
      start = nchars-self.max_length-self.scroll_pos
    else:
      start = self.scroll_pos
    end = start+self.max_length
    return max(0, start), min(end, nchars)
  def get_text(self):
    if self.use_markup:
      text = [self.margin_left]
      if self.prefix_enabled:
        text.extend(self.full_text_prefix.get_array())
      nchars = self.full_text_inner.nchars()
      if self.max_length > 0 and nchars > self.max_length:
        start, end = self.calc_scroll_slice(nchars)
        text.extend(self.full_text_inner.get_array_sliced(start, end))
      else:
        text.extend(self.full_text_inner.get_array())
      if self.suffix_enabled:
        text.extend(self.full_text_suffix.get_array())
      text.append(self.margin_right)
    else:
      text = [self.margin_left]
      if self.prefix_enabled:
        text.append(self.full_text_prefix)
      nchars = len(self.full_text_inner)
      if self.max_length > 0 and nchars > self.max_length:
        start, end = self.calc_scroll_slice(nchars)
        text.append(self.full_text_inner[start:end])
      else:
        text.append(self.full_text_inner)
      if self.suffix_enabled:
        text.append(self.full_text_suffix)
      text.append(self.margin_right)
    return unicode('').join(text)
  def make_json(self):
    json = ['{']
    text = dumps(self.get_text())
    json.append('"full_text":')
    json.append(text)
    if self.short_text != None:
      if self.use_markup:
        text = self.short_text.get_string()
      else:
        text = self.short_text
      json.append(', "short_text":')
      json.append(dumps(text))
    for i in self.options.iteritems():
      json.append(', ')
      json.append(dumps(i[0]))
      json.append(':')
      json.append(dumps(i[1]))
    json.append('}')
    for i in self.sub_blocks:
      if i.enabled:
        json.append(', ')
        json.append(i.make_json())
    json.append('\n')
    return ''.join(json)
  def update_scroll(self):
    if not (self.enabled and self.scroll_enabled):
      return
    old_scroll_pos = self.scroll_pos
    if self.use_markup:
      nchars = self.full_text_inner.nchars()
    else:
      nchars = len(self.full_text_inner)
    max_scroll_pos = nchars-self.max_length
    if max_scroll_pos <= 0:
      self.reset_scroll()
      if old_scroll_pos != 0:
        self.mark_dirty()
      return
    if self.scroll_pos >= max_scroll_pos:
      self.scroll_pos = max_scroll_pos
      self.scroll_ticks += 1
      if self.scroll_ticks > self.scroll_wait_time[self.scroll_dir]:
        self.scroll_pos = 0
        self.scroll_dir = (self.scroll_dir+1)%2
        self.scroll_ticks = 0
    else:
      self.scroll_ticks += 1
      if self.scroll_ticks > self.scroll_speed:
        self.scroll_pos += 1
        self.scroll_ticks = 0
    if self.scroll_pos != old_scroll_pos:
      self.mark_dirty()
  def update(self, use_json=None):
    if use_json == None:
      use_json = self.use_json
    if not self.dirty_flag:
      return
    self.dirty_flag.set_dirty(False)
    for i in self.sub_blocks:
      i.dirty_flag.set_dirty(False)
    if not self.enabled:
      return '{}\n' if use_json else '\n'
    if use_json:
      json = self.make_json()
    else:
      json = self.get_text()+'\n'
    return json
  def tick(self, ups):
    return -1
  def set_custom_options(self, **kw):
    pass
  def on_click(self, button, subblk, should_tick=True):
    return False
  def main(self, ups, out):
    import time
    sleep = 0
    self.pre_init_block()
    self.init_block()
    while True:
      if sleep == 0:
        sleep = self.tick(ups)
      elif sleep > 0:
        sleep -= 1
      self.update_scroll()
      text = self.update()
      if text != None:
        out.write(text)
        out.flush()
      time.sleep(1.0/ups)

class StyledBlock(Block):
  def __init__(self, use_json=True):
    Block.__init__(self, True, use_json)
    self.icon_text_color = '#000000'
    self.set_border_color = False
    self.margin_size_left = 0
    self.margin_size_right = 0
    self.ICON_PRE  = ' '
    self.ICON_POST = '\xe2\x94\x82'
    self.full_text_prefix.append(' ? ', lang="uf_UF",
                                 foreground=self.icon_text_color,
                                 background='#ffffff')
    self.full_text_prefix.append(' ')
    self.size_width = 4
    self.editable_attrs = \
      {'size_width', 'icon_text_color', 'set_border_color', \
       'prefix_enabled', 'margin_size_left', 'margin_size_right'}
  def set_icon(self, icon):
    self.full_text_prefix[0].set_content(self.ICON_PRE+icon+self.ICON_POST)
  def set_icon_color(self, color):
    self.full_text_prefix[0].set_arg('background', color)
    self.options['color'] = color
    if self.set_border_color:
      self.options['border'] = color
  def set_custom_options(self, **kw):
    #print kw
    Block.set_custom_options(self, **kw)
    if 'icon' in kw:
      self.set_icon(kw['icon'])
    for i in kw:
      if i in self.editable_attrs:
        setattr(self, i, kw[i])
      iu = i.upper()
      if hasattr(self, iu):
        setattr(self, iu, kw[i])
  def pre_init_block(self):
    Block.pre_init_block(self)
    self.full_text_prefix[0].set_arg('foreground', self.icon_text_color)
    self.margin_left  = ' '*self.margin_size_left
    self.margin_right = ' '*self.margin_size_right
  def fmt_size(self, n, base=1024):
    unit = 0
    while n >= base or n >= 10**self.size_width:
      n /= float(base)
      unit += 1
    w = self.size_width-2
    nn = n
    while nn >= 10.0-5*0.1**(self.size_width-1):
      nn /= 10
      w -= 1
    if w < 0:
      w = 0
    fmt = '{0:0.'+str(w)+'f}'
    return (fmt.format(n)+'BkMGTPEZY'[unit]).rjust(self.size_width+1)
